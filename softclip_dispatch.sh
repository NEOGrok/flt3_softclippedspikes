#!/usr/bin/env bash

for f in /home/projects/cu_10184/people/frakan/projects/PTH/data/bam/PTH0146-CMP0018*_dub_bqsr.bam; do
  echo "$f"
  qsub -N "$(basename $f)" -v "BAM=$f,OUTDIR=./out,MC=15" ./softclip_job.sh && sleep 1
done


