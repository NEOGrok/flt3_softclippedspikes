#!/usr/bin/env bash
### Note: No commands may be executed until after the #PBS lines
### Account information
#PBS -W group_list=cu_10184 -A cu_10184
###PBS -W group_list=cu_10145 -A cu_10145
### Job name (comment out the next line to get the name of the script used as the job name)
#PBS -N PTH_test
### Output files (comment out the next 2 lines to get the job name used instead)
#PBS -e test.err
#PBS -o test.log
### Only send mail when job is aborted or terminates abnormally
#PBS -m n
### Number of nodes
#PBS -l nodes=1:ppn=2
### Memory
#PBS -l mem=4gb
### Requesting time - format is <days>:<hours>:<minutes>:<seconds> (here, 12 hours)
#PBS -l walltime=02:00:00
### Forward X11 connection (comment out if not needed)
###PBS -X

# Go to the directory from where the job was submitted (initial directory is $HOME)
echo Working directory is $PBS_O_WORKDIR
cd $PBS_O_WORKDIR

### Here follows the user commands:
# Define number of processors
NPROCS=`wc -l < $PBS_NODEFILE`
echo This job has allocated $NPROCS nodes

# Load all required modules for the job
set +eu
eval "$(conda shell.bash hook)" > /dev/null 2>&1
conda activate /home/projects/cu_10184/people/frakan/conda/PTH > /dev/null 2>&1
set -ue

module load tools ngs
#module load parallel/20200922
#module load gcc
#module load intel/perflibs
#module load R/4.0.3
module load samtools/1.11

SAMPLE=$(basename $BAM)
SAMPLE=${SAMPLE%%_*}
echo $BAM >> tmp.log
echo $SAMPLE >> tmp.log
REF_GEN=/home/projects/cu_10184/people/frakan/refs/genome/ensembl/chr_only/Homo_sapiens.GRCh38_chrOnly.fa

/home/projects/cu_10184/people/frakan/projects/flt3_softclippedspikes/softclip_run.sh -i $BAM -o $OUTDIR -s $SAMPLE -g $REF_GEN -m $MC

